# BikeHike

<picture>
  <source media="(prefers-color-scheme: dark)" srcset="bikehike-white.svg">
  <source media="(prefers-color-scheme: light)" srcset="bikehike-cyan.svg">
  <img src="bikehike-cyan.svg" width="120" alt="BikeHike Cyan">
</picture>

#### Optimized SVG Logo

- SVG logo BikeHike Cyan (200x57 px)
- SVG logo BikeHike White (200x57 px)
- SVG logo BikeHike Service (200x140 px)
- SVG logo BikeHike CogWheel (200x96 px)
- SVG logo BikeHike Driver (200x200 px)
